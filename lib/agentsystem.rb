require 'rubygems'
require 'hpricot'
require 'agentsystembasics.rb'
include Jabber

#Network information distribution agent
class InformationDistributor < BasicAgent

  attr_accessor :modules

  #Gimme a new InformationDistributor
  def initialize username, password, host='krusty.techfak.uni-bielefeld.de', port=9092
    #this will connect/auth/register(if necessary) and add all the callbacks
    super(username, password, host, port)
  end

  #Add modules to handle incoming signals
  def addmodules
    #module that takes care of status messages
    @modules['status'] = StatusMessageHandler.new self
  end

  #Handles a roster event
  def rosterEvent event
    begin
      event.each {|k,v| @log.debug "#{@username} - RE: #{k}: #{v}"}
    rescue
      @log.error "#{@username} - rosterEvent error"
    end
  end

  #Handles a message event
  def messageEvent event
    begin
      event.each { |k,v| parsexmlmessage v if v.subject.eql? "XML" if k.eql? :message }
    rescue
      @log.error "#{@username} - messageEvent error"
    end
  end

  #Handles a pubsub event
  def pubsubEvent event
    begin
      event.each {|k,v| @log.debug "#{@username} - PSE: #{k}: #{v}"}
    rescue
      @log.error "#{@username} - pubsubEvent error"
    end
  end
end

#A message handler to handle status messages
class StatusMessageHandler < BasicMessageHandler
  def initialize distributor
    super(distributor)
    @modulename = "status"
    @modulenode = "home/#{@distributor.host}/#{@distributor.username}/#{@modulename}"
    @log.debug "Modulenode: #{@modulenode}!"
    #create the pubsub node for this module
    @distributor.createnode @modulename
    @log.info "New #{@modulename} created!"
  end

  #method to handle incoming signals
  def signal sender,senderclass,signal,value
    begin
      @log.debug "#{@modulename}: sender: #{sender} signal: #{signal} value: #{value}"
      client_connect sender if signal.eql? 'connect'
      client_addservice sender,value if signal.eql? 'addservice'
      client_removeservice sender,value if signal.eql? 'removeservice'
    rescue
      @log.error "#{@modulename}: signal handling error"
    end
  end

  #gets called when a client connects
  def client_connect sender
    begin
      unless @clients.has_key? sender
        @clients[sender] = {:jid => sender, :status => 'connected'}
      end
    rescue
      @log.error "#{@modulename}: client connect handling error"
    end
  end

  #add a service offered by a client
  def client_addservice sender,xmlvalue
    begin
      client_connect sender unless @clients.has_key? sender
      xml = Hpricot::XML(xmlvalue.to_s)
      name,type,value = "","",""
      (xml/"//service").each { |node| name,type,value = node["name"],node["type"],node.inner_text }
      @clients[sender][:services] = {} unless @clients[sender].has_key? :services
      @clients[sender][:services][name] = {:name => name, :type => type, :value => value, :xml => xmlvalue} unless @clients[sender][:services].has_key? name
      updateservices
    rescue
      @log.error "#{@modulename}: addservice handling error"
    end
  end

  #remove a service offered by a client
  def client_removeservice sender,value
    begin
      @clients[sender][:services].delete value if @clients[sender][:services].has_key? value if @clients[sender].has_key? :services if @clients.has_key? sender
      updateservices
    rescue
      @log.error "#{@modulename}: removeservice handling error"
    end
  end

  #update the service information
  def updateservices
    @clients.each do |id,clientinfo|
      if clientinfo.has_key? :services
        clientinfo[:services].each do |id,serviceinfo|
          @log.debug "#{@modulename}: service: #{id} by #{clientinfo[:jid]}"
          #publish the information to our node
          @distributor.publish2node @modulenode, serviceinfo[:xml]
        end
      end
    end
  end
end

#AS Agent implementation
class Agent < BasicAgent
  #Gimme a new Agent
  def initialize username, password, host='krusty.techfak.uni-bielefeld.de', port=9092
    super(username, password, host, port)
    addservices
    subscribe2distributor @distributor
  end

  #add the services that we provide
  def addservices
    #if we're in debug mode, add a debug service
    add_service @distributor,"debug","pubsub","home/#{@host}/#{@username}/debug" if @log.level == Logger::DEBUG
  end

  #Handles a roster event
  def rosterEvent event
    begin
      event.each {|k,v| @log.debug "#{@username} - RE: #{k}: #{v}"}
    rescue
      @log.error "#{@username} - rosterEvent error"
    end
  end

  #Handles a message event
  def messageEvent event
    begin
      event.each {|k,v| @log.debug "#{@username} - ME: #{k}: #{v}"}
    rescue
      @log.error "#{@username} - messageEvent error"
    end
  end

  #Handles a pubsub event
  def pubsubEvent event
    begin
      event.each {|k,v| @log.debug "#{@username} - PSE: #{k}: #{v}"}
    rescue
      @log.error "#{@username} - pubsubEvent error"
    end
  end
end
