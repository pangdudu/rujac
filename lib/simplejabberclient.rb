require 'jabberclient.rb'
#SimpleJabberClient implementation
class SimpleJabberClient < JabberClient
  #Gimme a new SimpleJabberClient
  def initialize username, password, host='krusty.techfak.uni-bielefeld.de', port=9092
    #this will connect/auth/register(if necessary) and add all the callbacks
    super(username, password, host, port)
  end

  #Handles a roster event
  def rosterEvent event
    begin
      event.each {|k,v| @log.debug "#{k}: #{v}"}
    rescue
      @log.error "#{@username} - rosterEvent error"
    end
  end

  #Handles a message event
  def messageEvent event
    begin
      event.each {|k,v| @log.debug "#{k}: #{v}"}
    rescue
      @log.error "#{@username} - messageEvent error"
    end
  end

  #Handles a pubsub event
  def pubsubEvent event
    begin
      event.each {|k,v| @log.debug "#{k}: #{v}"}
    rescue
      @log.error "#{@username} - pubsubEvent error"
    end
  end
end