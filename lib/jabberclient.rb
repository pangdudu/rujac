require "rubygems"
require "xmpp4r"
require 'xmpp4r/roster'
require "xmpp4r/pubsub"
require "xmpp4r/pubsub/helper/servicehelper.rb"
require "xmpp4r/pubsub/helper/nodebrowser.rb"
require "xmpp4r/pubsub/helper/nodehelper.rb"
include Jabber

#Rather complex JabberClient the SimpleJabberClient derives from
class JabberClient

  attr_accessor :log
  attr_accessor :username,:password,:host,:port
  attr_accessor :client,:roster,:pubsub

  #Construct a new JabberClient
  def initialize username, password, host='krusty.techfak.uni-bielefeld.de', port=9092
    @log = Logger.new(STDOUT)
    @log.level = Logger::DEBUG
    #if we are the controller, this boolean but be true
    @username,@password,@host,@port = username,password,host,port
    @jid = "#{@username}@#{@host}/SLAC Information Client"
    @pubsubservice = "pubsub.#{@host}"
    @client = Client.new(JID.new(@jid))
    connect
  end

  #connect to server is always the first step to be awesome
  def connect
    begin
      @client.connect(@host, @port)
    rescue
      @log.error "#{@username} - Connect failed!"
    end
    begin #in case this doesn't work register a new user
      @client.auth(@password)
    rescue
      @client.close
      begin
        @client.connect(@host, @port)
        @client.register(@password, {'nick'=>@username,'name'=>@username})
      rescue
        @log.error "#{@username} - Connect/Auth/Register failed, wrong password?"
        exit
      end
    end
    @presence = Jabber::Presence.new
    @presence.show = :chat
    @presence.status = "Up and running!"
    @client.send(@presence)
    sleep(1)
    #get a roster helper
    @roster = Roster::Helper.new(@client)
    #spend pubsub a margaritha
    @pubsub = PubSub::ServiceHelper.new(@client, @pubsubservice)
    sleep(1)
    addcallbacks
    @log.info "#{@username} - Connected, subscribed and ready to go!"
  end

  #will add all callbacks implemented in this class
  def addcallbacks
    addrostercallbacks
    addmessagecallback
    @log.info "#{@username} - All callbacks added. We're good to go!"
  end

  #adds all roster callbacks
  def addrostercallbacks
=begin
    #adds callback for roster query, gets called if 'something' happens
    @roster.add_query_callback do |stanza|
      event = {:timestamp => Time.now,:type => 'roster_query', :stanza => stanza}
      rosterEvent event
    end
=end
    #adds a callback for presence changes
    @roster.add_update_callback do |oldrosteritem,newrosteritem|
      event = {:timestamp => Time.now,:type => 'roster_update', :oldrosteritem => oldrosteritem, :newrosteritem => newrosteritem}
      rosterEvent event
    end
    #adds a callback for presence changes
    @roster.add_presence_callback do |rosteritem, oldpresence, newpresence|
      event = {:timestamp => Time.now,:type => 'roster_presence', :rosteritem => rosteritem, :oldpresence => oldpresence, :newpresence => newpresence}
      rosterEvent event
    end
    #adds callback for incoming subscriptions
    @roster.add_subscription_callback do |item,stanza|
      event = {:timestamp => Time.now,:type => 'roster_subscription', :rosteritem => item, :stanza => stanza}
      rosterEvent event
    end
    #adds callback for incoming subscriptions requests
    @roster.add_subscription_request_callback do |item,presence|
      #TODO: only do this if the user wants it. :)
      @roster.accept_subscription(presence.from)
      subscribe2jid presence.from
      @log.info "#{@username} - Accepted request from: #{presence.from.to_s}"
      event = {:timestamp => Time.now,:type => 'roster_subscription_request', :rosteritem => item, :presence => presence}
      rosterEvent event
    end
  end

  #adds a callback for incoming messages
  def addmessagecallback
    @client.add_message_callback do |message|
      event = {:timestamp => Time.now,:type => 'message', :message => message}
      messageEvent event
    end
  end

  #subscribe to a pubsub node
  def subscribe2node node
    begin
      @pubsub.subscribe_to(node)
      @pubsub.add_event_callback do |event|
        event = {:timestamp => Time.now,:type => 'pubsub_node', :event => event}
        pubsubEvent event
      end
    rescue
      @log.error "#{@username} - Couldn't subscribe to : #{node}"
    end
  end

  #Handles a roster event
  #if you derive from this class, you want to implement this method
  def rosterEvent event
    begin
      event.each {|k,v| @log.debug "#{k}: #{v}"}
    rescue
      @log.error "#{@username} - rosterEvent error"
    end
  end

  #Handles a message event
  #if you derive from this class, you want to implement this method
  def messageEvent event
    begin
      event.each {|k,v| @log.debug "#{k}: #{v}"}
    rescue
      @log.error "#{@username} - messageEvent error"
    end
  end

  #Handles a pubsub event
  #if you derive from this class, you want to implement this method
  def pubsubEvent event
    begin
      event.each {|k,v| @log.debug "#{k}: #{v}"}
    rescue
      @log.error "#{@username} - pubsubEvent error"
    end
  end

  #subscribe to another jid and his nodes
  def subscribe2jid jid
    begin
      request = Jabber::Presence.new
      request.to = jid
      request.type = :subscribe  # This one!
      @client.send(request)
      sleep 1
      @roster.add(Jabber::Roster::RosterItem.new(jid,jid,:both,:subscribed))
    rescue
      @log.error "#{@username} - #{jid} couldn't be added."
    end
  end

  # sends a message to a client
  def sendmessage to, subject, body
    begin
      @log.debug "#{@username} - Sending message to #{to}"
      m = Message.new(to, body).set_type(:normal).set_id('1').set_subject(subject)
      @client.send m
    rescue
      @log.error "#{@username} - Message couldn't be send to #{to}"
    end
  end

  #create our pubsub node
  def createnode node
    begin
      @pubsub.create_node("home/#{@host}/#{@username}/")
    rescue
    end #if this fails, that normaly means the node has already been created
    begin
      @pubsub.create_node(node)
    rescue #if this fails, that normaly means the node has already been created
      @log.debug "#{@username} - node #{node} already created, skipping."
    end
  end

  #publish something to one of our nodes
  def publish2node node,text
    # create item
    item = Jabber::PubSub::Item.new
    xml = REXML::Element.new("Update")
    xml.text = text
    item.add(xml)
    #publish item
    #@log.debug "Trying to publish this to node: #{text}"
    @pubsub.publish_item_to(node, item)
    @log.debug "#{@username} - Published this to node: #{text}"
  end

  #show our current roster
  def showroster
    @log.debug "#{@username} - Roster Info:"
    @roster.items.each do |jid,node|
      @log.info "#{@username} - Roster member: #{jid} status: #{node.inspect}"
    end
  end

  #output subscriptions
  def getsubscriptions
    subscriptions = @pubsub.get_subscriptions_from_all_nodes()
    @log.debug "#{@username} - subscriptions: #{subscriptions}"
  end
end
