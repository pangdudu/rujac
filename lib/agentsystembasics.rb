require 'rubygems'
require 'hpricot'
require 'jabberclient.rb'


#Agent system agent basic class
class BasicAgent < JabberClient
  attr_accessor :distributor,:modules,:services

  #Gimme a new BasicAgent
  def initialize username, password, host='krusty.techfak.uni-bielefeld.de', port=9092
    #this will connect/auth/register(if necessary) and add all the callbacks
    super(username, password, host, port)
    @distributor = "distributor@" + host
    @modules = {}
    @services = {}
    @log.info "#{@username} - Distributor is #{@distributor}"
    inform_distributor @distributor
  end

  #tell the distributor that we offer a service
  def add_service distributor,name,type,value
    subject = "XML"
    service = "<service name='#{name}' type='#{type}'>#{value}</service>"
    body = "<xmlmessage senderclass='#{self.class.to_s}' messagetype='status' signal='addservice'>#{service}</xmlmessage>"
    @log.debug "#{@username} - add_service XML: #{body}"
    sendmessage distributor, subject, body
    @services[name] = {:name => name, :type => type, :value => value }
    #create the pubsub node if it is a pubsub service we're offering
    createnode value if type.eql? "pubsub"
  end

  #tell the distributor that we remove a service
  def remove_service distributor,name
    subject = "XML"
    body = "<xmlmessage senderclass='#{self.class.to_s}' messagetype='status' signal='removeservice'>#{name}</xmlmessage>"
    sendmessage distributor, subject, body
  end

  #inform the distributor that we're connected
  def inform_distributor distributor
    subscribe2jid distributor
    subject = "XML"
    body = "<xmlmessage senderclass='#{self.class.to_s}' messagetype='status' signal='connect'>online</xmlmessage>"
    sendmessage distributor, subject, body
  end

  #parse an xml message using hpricot
  def parsexmlmessage message
    begin
      sender,senderclass,messagetype,signal = message.from.to_s, "", "",""
      #now we parse the xml using hpricot
      xml = Hpricot::XML(message.body)
      (xml/"/xmlmessage").each { |node| senderclass, messagetype, signal = node["senderclass"],node["messagetype"], node["signal"] }
      forward2module sender,senderclass,messagetype,signal,xml
    rescue
      @log.error "#{@username} - XML parsing error"
    end
  end

  #Forward a signal to the corresponding module
  def forward2module sender,senderclass,messagetype,signal,value
    #do we have a module to handle this?
    if @modules.has_key? messagetype
      mod = @modules[messagetype]
      mod.signal sender,senderclass,signal,value
    end
  end

  def subscribe2distributor distributor
    node = "home/#{@host}/#{@distributor}/status"
    @log.debug "#{@username} - Subscribed to node: #{node}"
    #subscribe to distributor network status updates
    subscribe2node node
  end

  #Handles a roster event
  def rosterEvent event
    begin
      event.each {|k,v| @log.debug "#{@username} - RE: #{k}: #{v}"}
    rescue
      @log.error "#{@username} - rosterEvent error"
    end
  end

  #Handles a message event
  def messageEvent event
    begin
      event.each {|k,v| @log.debug "#{@username} - ME: #{k}: #{v}"}
    rescue
      @log.error "#{@username} - messageEvent error"
    end
  end

  #Handles a pubsub event
  def pubsubEvent event
    begin
      event.each {|k,v| @log.debug "#{@username} - PSE: #{k}: #{v}"}
    rescue
      @log.error "#{@username} - pubsubEvent error"
    end
  end
end

#A simple message handler to handle incoming signals
class BasicMessageHandler
  attr_accessor :distributor
  attr_accessor :clients
  attr_accessor :modulename

  def initialize distributor
    @log = Logger.new(STDOUT)
    @log.level = Logger::DEBUG
    @distributor = distributor
    @modulename = "BasicMessageHandler"
    @clients = {}
  end

  #method to handle incoming signals
  def signal sender,senderclass,signal,value
    begin
      @log.debug "#{@modulename}: sender: #{sender} senderclass: #{senderclass} signal: #{signal} value: #{value}"
    rescue
      @log.error "#{@modulename}: signal handling error"
    end
  end
end